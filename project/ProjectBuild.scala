import sbt._
import sbt.Keys._

object ProjectBuild extends Build {
  override lazy val settings = super.settings ++ Seq(
    name := "conversion-bson",
    organization := "com.github.nyavro",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.11.4",
    autoCompilerPlugins := true,
    artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
      artifact.name + "-" + module.revision + "." + artifact.extension
    },
    resolvers ++= Seq(
      "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
      "typesafe" at "http://repo.typesafe.com/typesafe/releases",
      "releases" at "http://oss.sonatype.org/content/repositories/releases"
    ),
    libraryDependencies ++= Seq(
      "org.scalatest" % "scalatest_2.11" % "2.2.3" % "test" withSources() withJavadoc(),
      "org.reactivemongo" % "reactivemongo_2.11" % "0.10.5.0.akka23",
      "junit" % "junit" % "4.10" withSources() withJavadoc(),
      "org.specs2" % "specs2_2.11" % "2.3.12" withSources() withJavadoc(),
      "org.scalamock" %% "scalamock-scalatest-support" % "3.2.1" % "test",
      "com.nyavro" %% "conversion-core" % "1.0"
    ),
    scalacOptions ++= Seq("-deprecation", "-unchecked")
  )
}
